##############################################################
#### Biotracker: backgroundSubtraction
##############################################################

include(biotracker-utility/TrackerPlugin)

set(target python.bio_tracker)

add_behavior_plugin(${target}
    "PluginContext.cpp"
    "Config.cpp"
    "BioTrackerPlugin.cpp"
    "View/TrackedElementView.cpp"
    "View/TrackerParameterView.cpp"
    "Model/BioTrackerTrackingAlgorithm.cpp"
    "Model/TrackerParameter.cpp"
    "Model/TrackedComponents/TrackedElement.cpp"
    "Model/TrackedComponents/TrackedTrajectory.cpp"
    "Model/TrackedComponents/pose/FishPose.cpp"
    "Controller/ControllerTrackedComponent.cpp"
    "Controller/ControllerTrackingAlgorithm.cpp"
)

install(TARGETS ${target} OPTIONAL DESTINATION .)
find_package(Qt5 REQUIRED COMPONENTS Xml Network)
find_package( ZeroMQ REQUIRED )
target_link_libraries (${target} Qt5::Xml Qt5::Network rt zmq)
