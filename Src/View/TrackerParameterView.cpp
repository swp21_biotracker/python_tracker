#include "TrackerParameterView.h"
#include "ui_TrackerParameterView.h"

#include <iostream>
#include <QFileDialog>
#include <QString>

TrackerParameterView::TrackerParameterView(QWidget *parent, IController *controller, IModel *model) :
    IViewWidget(parent, controller, model),
    _ui(new Ui::TrackerParameterView)
{
    _ui->setupUi(this);
    QObject::connect(_ui->model_path_browse, SIGNAL(clicked()), this, SLOT(on_model_path_browse()));
}

void TrackerParameterView::on_model_path_browse() {
    QString dir = QFileDialog::getExistingDirectory(this, tr("Choose Model"),
                                                "/home",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);
    _ui->model_path_line->setText(dir);
    TrackerParameter *parameter = qobject_cast<TrackerParameter *>(getModel());
    parameter->setModelPath(dir);
    Q_EMIT parametersChanged();
}

TrackerParameterView::~TrackerParameterView()
{
    delete _ui;
}

void TrackerParameterView::getNotified()
{
}
