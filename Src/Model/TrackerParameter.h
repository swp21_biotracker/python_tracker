#ifndef TRACKERPARAMETER_H
#define TRACKERPARAMETER_H


#include "Interfaces/IModel/IModel.h"
#include "../Config.h"
#include <optional>

class TrackerParameter : public IModel
{
    Q_OBJECT
public:
    TrackerParameter(QObject *parent = 0);

	std::string getNewSelection() { return _newSelection; };
	void setNewSelection(std::string x) {
		_newSelection = x;
	}

    void setModelPath(QString x) {
        _model_path = x;
        Q_EMIT notifyView();
    }

    QString getModelPath() { return _model_path; };

private:
    QString _model_path;

	std::string _newSelection;
	Config *_cfg;
};

#endif // TRACKERPARAMETER_H
