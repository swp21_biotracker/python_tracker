#include "TrackerParameter.h"
#include "../Controller/ControllerTrackingAlgorithm.h"

TrackerParameter::TrackerParameter(QObject *parent) :
    IModel(parent)
{
	_cfg = static_cast<ControllerTrackingAlgorithm*>(parent)->getConfig();
    Q_EMIT notifyView();
}
