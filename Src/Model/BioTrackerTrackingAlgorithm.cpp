#include "BioTrackerTrackingAlgorithm.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <chrono>
#include <future>
#include <iostream>
#include <string>
#include <zmq.hpp>

#include "TrackedComponents/pose/FishPose.h"
#define JSON_USE_IMPLICIT_CONVERSIONS 0
#include "json.hpp"

using json = nlohmann::json;

BioTrackerTrackingAlgorithm::BioTrackerTrackingAlgorithm(IController *parent,
                                                         IModel *parameter,
                                                         IModel *trajectory)
    : IModelTrackingAlgorithm(parent) {
    _cfg = static_cast<ControllerTrackingAlgorithm *>(parent)->getConfig();
    _TrackingParameter = (TrackerParameter *)parameter;
    _TrackedTrajectoryMajor = (BST::TrackedTrajectory *)trajectory;

    _noFish = -1;

    _lastImage = nullptr;
    _lastFramenumber = -1;
}

BioTrackerTrackingAlgorithm::~BioTrackerTrackingAlgorithm() { stop_python(); }

void BioTrackerTrackingAlgorithm::init_shared_memory() {
    json j = {
        {"type", "request_shared_memory"},
        {"width", _imageX},
        {"height", _imageY},
    };
    _sock.send(zmq::buffer(j.dump()));
    auto res = _sock.recv(_zmq_msg, zmq::recv_flags::none);
    auto msg = json::parse(_zmq_msg.to_string_view());
    std::string shm_path = msg["path"].get<std::string>();
    int shm_len = _imageX * _imageY * sizeof(float);

    int fd = shm_open(shm_path.c_str(), O_RDWR, 0);
    if (fd == -1) {
        throw "shm_open";
    }

    void *shm_buf =
        mmap(NULL, shm_len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);

    if (shm_buf == MAP_FAILED) {
        throw "mmap";
    }

    _shm_img = (float *)shm_buf;
}

void BioTrackerTrackingAlgorithm::stop_python() {
    if (_python_process.has_value()) {
        _python_process_group.terminate();
        _python_process.value().wait();
    }
}

void BioTrackerTrackingAlgorithm::start_python() {
    stop_python();
    auto model_path = _TrackingParameter->getModelPath().toStdString();
    _python_process = boost::process::child(
        boost::process::search_path("python3"),
        boost::process::args({"-c",
                              "from biotracker import BiotrackerAdapter;"
                              "BiotrackerAdapter('" +
                                  model_path +
                                  "'"
                                  ",verbose=True).run();"}),
        _python_process_group);
    fprintf(stderr, "Adapter started\n");

    _sock = zmq::socket_t(_ctx, zmq::socket_type::req);
    _sock.connect("ipc:///tmp/biotracker.python.zmq");
}

void BioTrackerTrackingAlgorithm::receiveAreaDescriptorUpdate(
    IModelAreaDescriptor *areaDescr) {
    _areaInfo = areaDescr;
}

void BioTrackerTrackingAlgorithm::receiveParametersChanged() {
    if (_TrackingParameter->getModelPath() != NULL) {
        start_python();
    }

    if (_lastFramenumber >= 0 && _lastImage && !_lastImage->empty()) {
        doTracking(_lastImage, _lastFramenumber);
    }
}

void BioTrackerTrackingAlgorithm::doTracking(std::shared_ptr<cv::Mat> p_image,
                                             uint framenumber) {
    _lastImage = p_image;
    _lastFramenumber = framenumber;

    // dont do nothing if we ain't got an image
    if (p_image->empty()) {
        return;
    }

    // skip frame if model is not yet loaded
    if (!_python_process) {
        fprintf(stderr, "Adapter not running!\n");
        return;
    }

    if (_imageX != p_image->size().width || _imageY != p_image->size().height) {
        _imageX = p_image->size().width;
        _imageY = p_image->size().height;
        fprintf(stderr, "Initializing shared memory...\n");
        init_shared_memory();
        fprintf(stderr, "Done\n");
        Q_EMIT emitDimensionUpdate(_imageX, _imageY);
    }

    // Refuse to run tracking if we have no area info...
    if (_AreaInfo == nullptr) {
        fprintf(stderr, "Missing area info\n");
        Q_EMIT emitTrackingDone(framenumber);
        return;
    }

    std::chrono::system_clock::time_point start =
        std::chrono::system_clock::now();

    cv::Mat grayscale, float_img;
    cv::cvtColor(*p_image, grayscale, cv::COLOR_BGR2GRAY);
    grayscale.convertTo(float_img, CV_32F);
    float *img_data = float_img.ptr<float>(0);
    int img_data_len = float_img.cols * float_img.rows * sizeof(float);
    memcpy(_shm_img, img_data, img_data_len);

    json j = {
        {"type", "predict_frame"},
        {"frame_id", framenumber},
    };
    _sock.send(zmq::buffer(j.dump()));
    auto res = _sock.recv(_zmq_msg, zmq::recv_flags::none);
    auto msg = json::parse(_zmq_msg.to_string_view());

    std::vector<std::tuple<int, FishPose>> poses;
    for (auto pos : msg["data"]) {
        fprintf(stderr, "Here\n");
        auto pose = std::make_tuple(
            pos["id"].get<int>(),
            FishPose(_areaInfo,
                     cv::Point2f(pos["x"].get<int>(), pos["y"].get<int>()),
                     pos["orientation"].get<float>(),
                     1.0));  // FIXME: score
        poses.push_back(pose);
    }
    std::sort(poses.begin(), poses.end(),
              [](std::tuple<int, FishPose> a, std::tuple<int, FishPose> b) {
                  return std::get<0>(a) < std::get<0>(b);
              });

    // Insert new poses into data structure
    int trajNumber = 0;
    for (int i = 0; i < _TrackedTrajectoryMajor->size(); i++) {
        BST::TrackedTrajectory *t = dynamic_cast<BST::TrackedTrajectory *>(
            _TrackedTrajectoryMajor->getChild(i));
        if (t && t->getValid() && !t->getFixed()) {
            BST::TrackedElement *e =
                new BST::TrackedElement(t, "n.a.", t->getId());

            e->setFishPose(std::get<1>(poses[trajNumber]));
            e->setTime(start);
            t->add(e, framenumber);
            trajNumber++;
        }
    }

    std::string newSel = _TrackingParameter->getNewSelection();

    Q_EMIT emitChangeDisplayImage("Original");
    Q_EMIT emitTrackingDone(framenumber);
}
