#include "FishPose.h"

FishPose::FishPose(cv::Point2f pos_cm , cv::Point pos_px, float rad, float deg, float width, float height, float score) :
	_position_cm(pos_cm),
	_position_px(pos_px),
	_radAngle(rad),
	_degAngle(deg),
	_width(width),
	_height(height),
	_score(score)
{
	assert(_degAngle >= -360.0f && _degAngle <= 360.0f);
	time(&_timev);
}

FishPose::FishPose(IModelAreaDescriptor *_areaInfo, cv::Point pos_px, float rad, float score) :
	_position_px(pos_px),
	_radAngle(rad),
	_width(1.0),
	_height(1.0),
	_score(score)
{
	assert(_degAngle >= -360.0f && _degAngle <= 360.0f);
	time(&_timev);
    _position_cm = _areaInfo->pxToCm(_position_px);
    _degAngle = _radAngle * (180.0 / CV_PI);
}

std::string FishPose::toString(bool rectified)
{
	std::ostringstream out;
	if(rectified == false)
		out << _position_px.x << ";" << _position_px.y << ";" << _degAngle << ";" << _radAngle;
	else
		out << _position_cm.x << ";" << _position_cm.y << ";" << _degAngle << ";" << _radAngle;
	return out.str();
}
