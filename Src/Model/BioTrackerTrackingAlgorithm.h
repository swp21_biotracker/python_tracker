#ifndef BIOTRACKERTRACKINGALGORITHM_H
#define BIOTRACKERTRACKINGALGORITHM_H


#include "Interfaces/IModel/IModel.h"

#include "TrackerParameter.h"

#include <boost/process.hpp>
#include <opencv2/opencv.hpp>
#include "Interfaces/IModel/IModelTrackingAlgorithm.h"
#include "Interfaces/IModel/IModelDataExporter.h"
#include "TrackedComponents/TrackedElement.h"
#include "TrackedComponents/TrackedTrajectory.h"
#include "../Controller/ControllerTrackingAlgorithm.h"
#include "Interfaces/IModel/IModelAreaDescriptor.h"
#include <iostream>
#include <zmq.hpp>

#include "../Config.h"

class BioTrackerTrackingAlgorithm : public IModelTrackingAlgorithm
{
    Q_OBJECT
public:
    BioTrackerTrackingAlgorithm(IController *parent, IModel* parameter, IModel* trajectory);
	~BioTrackerTrackingAlgorithm();

Q_SIGNALS:
    void emitCvMatA(std::shared_ptr<cv::Mat> image, QString name);
	void emitDimensionUpdate(int x, int y);
	void emitTrackingDone(uint framenumber);

    // ITrackingAlgorithm interface
public Q_SLOTS:
	void doTracking(std::shared_ptr<cv::Mat> image, uint framenumber) override;
	void receiveAreaDescriptorUpdate(IModelAreaDescriptor *areaDescr);
    void receiveParametersChanged();

private:
    void init_shared_memory();
    void start_python();
    void stop_python();

    BST::TrackedTrajectory* _TrackedTrajectoryMajor;
	TrackerParameter* _TrackingParameter;
	IModelAreaDescriptor* _AreaInfo;

	int _noFish;

    std::optional<boost::process::child> _python_process;
    boost::process::group _python_process_group;

	int _imageX;
	int _imageY;

    std::shared_ptr<cv::Mat> _lastImage;
    uint _lastFramenumber;
	Config *_cfg;

    zmq::context_t _ctx;
    zmq::socket_t _sock;
    zmq::message_t _zmq_msg;
    float *_shm_img;
	IModelAreaDescriptor* _areaInfo;
};

#endif // BIOTRACKERTRACKINGALGORITHM_H
